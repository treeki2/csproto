package main

import (
	"os"
	"time"
	
	"google.golang.org/protobuf/compiler/protogen"
	"golang.org/x/tools/imports"
	
	"gd/cmd/csproto/templates"
)

// generate ...
func generate(plugin *protogen.Plugin, req generateRequest) error {
	// if req.Mode == outputModeSingleFile {
	// 	return generateSingle(plugin, req)
	// }
	return generatePerMessage(plugin, req)
}



func generatePerMessage(plugin *protogen.Plugin, req generateRequest) (err error) {
	var (
		now    = time.Now().UTC()
		pwd, _ = os.Getwd()
	)
	allMessages := templates.AllMessages(req.ProtoDesc)
	
	for _, msg := range allMessages {
		mTpl := templates.MessageTpl{
			Message            : msg,
			Name               : req.NameTemplate,
			APIVersion         : req.APIVersion,
			SpecialNames			 : req.SpecialNames,
			ProtoFile					 : req.ProtoDesc,
			Now                : now,
			Pwd                : pwd,
			EnableUnsafeDecode : req.EnableUnsafeDecode,
		}
		
		headContent := templates.Head("test")
		
		content := mTpl.PerMesssage("testP")
		fname := "genCsp__.testP.go"
		
		// result := plugin.NewGeneratedFile(fname, req.ProtoDesc.GoImportPath)
		// if _, err = result.Write([]byte(content)); err != nil {
		// 	return fmt.Errorf("error writing generated content for message %s to %q: %w", msg.Desc.FullName(), fname, err)
		// }
		destFile, err := os.Create(fname)
		if err != nil {
			return err
		}
		defer destFile.Close()
		
		out, err := imports.Process("", []byte(headContent + content), nil)
		if err != nil {
			return err
		}
		
		if _, err2 := destFile.Write(out); err2 != nil {
			return err2
		}
	}
	
	return err
}


