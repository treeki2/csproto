package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"text/template"
	
	"google.golang.org/protobuf/compiler/protogen"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/pluginpb"
	
	"gd/cmd/csproto/templates"
)

// options defines the supported configuration options for a generation request
type options struct {
	OutputPath string
	Debug      bool
	
	ContentTemplate string
	GetFuncMap      func(*protogen.File, bool) template.FuncMap
	
	apiVersion         protoAPIVersion
	filePerMessage     bool
	specialNames       templates.SpecialNames
	enableUnsafeDecode bool
}

// protoAPIVersion defines a string flag that can contain either "v1" or "v2"
type protoAPIVersion string

// String returns a string representation of v
func (v protoAPIVersion) String() string {
	return string(v)
}

// Set assigns the value of v from s or returns an error if s contains an invalid value.
func (v *protoAPIVersion) Set(s string) error {
	s = strings.ToLower(s)
	switch s {
	case "v1", "v2":
		*v = protoAPIVersion(s)
		return nil
	default:
		return fmt.Errorf("invalid Protobuf API version: %s", s)
	}
}


// run executes the code generator with the provided options applied
func run() {
	// setup default run options
	runOptions := options{
		specialNames: make(templates.SpecialNames),
	}
	
	// define our custom flags
	flags := flag.NewFlagSet("protoc-gen-fastmarshal", flag.ExitOnError)
	flags.Var(&runOptions.apiVersion, "apiversion", "the Protobuf API version to use (v1, v2)")
	flags.StringVar(&runOptions.OutputPath, "dest", "", "the path of the output file to be written")
	flags.BoolVar(&runOptions.Debug, "debug", false, "if true, enable verbose debugging output to stderr")
	flags.BoolVar(&runOptions.filePerMessage, "filepermessage", false, "if true, outputs a separate file for each message")
	flags.Var(&runOptions.specialNames, "specialname", "if set, specifies field names to be munged in the generated code")
	flags.BoolVar(&runOptions.enableUnsafeDecode, "enableunsafedecode", false, "if true, enables using unsafe code to decode strings for better perf")
	
	// load and run the generator
	genOptions := protogen.Options{
		ParamFunc: flags.Set,
	}
	// genOptions.Run(doGenerate(&runOptions))
	err := protogenLocalRun(genOptions, doGenerate(&runOptions))
  if err != nil {
		log.Fatal(err)
	}
}

func doGenerate(opts *options) func(*protogen.Plugin) error {
	return func(plugin *protogen.Plugin) error {
		if len(plugin.Files) == 0 {
			return fmt.Errorf("no files to generate, exiting")
		}
		
		if opts.apiVersion == "" {
			opts.apiVersion = protoAPIVersion("v1")
		}
		
		for _, protoFile := range plugin.Files {
			if !protoFile.Generate {
				continue
			}
			
			// account for per-message output mode
			// - default output template is "[protofile].pb.fm.go"
			// - file-per-message template is "[protofile]_[lower(messagename)].pb.fm.go"
			nameTemplate := protoFile.GeneratedFilenamePrefix + `.pb.fm.go`
			if opts.filePerMessage {
				nameTemplate = protoFile.GeneratedFilenamePrefix + `_{{.Message.Desc.Name | string | lower}}.pb.fm.go`
			}
			
			req := generateRequest{
				Mode:               outputModeSingleFile,
				ProtoDesc:          protoFile,
				NameTemplate:       nameTemplate,
				APIVersion:         opts.apiVersion.String(),
				SpecialNames:       opts.specialNames,
				EnableUnsafeDecode: opts.enableUnsafeDecode,
			}
			if opts.filePerMessage {
				req.Mode = outputModeFilePerMessage
			}
			if err := generate(plugin, req); err != nil {
				return err
			}
		}
		return nil
	}
}

//check: https://github.com/lyft/protoc-gen-star/blob/master/protoc-gen-debug/main.go

func protogenLocalRun(opts protogen.Options, f func(*protogen.Plugin) error) (err error) {
	//
	/* We run these shell commands in golang
	# check: https://github.com/lyft/protoc-gen-star/blob/master/protoc-gen-debug/main.go
	protoc \
	  --plugin=protoc-gen-debug=$GOPATH/bin/protoc-gen-debug \
	  --debug_out=".:." \
	  *.proto
	*/
	epath := os.Getenv("GOPATH")
	pwd, _ := os.Getwd()
	binFile := filepath.Join(epath, "/bin/protoc-gen-debug")
	debugOut :=  " --debug_out="+pwd+":"+pwd
	cmd := exec.Command("/usr/bin/sh", "-c", "protoc --plugin=protoc-gen-debug="+binFile+debugOut+" *.proto")
	// var execOut bytes.Buffer
	// var execErr bytes.Buffer
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Dir = pwd
	err = cmd.Run()
	if err != nil {
		// if execErr.String() == "ERROR 404: Not Found."{
		//   println("404 Not found")
		//   continue
		// } else {
		//   return err
		// }
		println("Error")
		// cmd = exec.Command("rm", "./code_generator_request.pb.bin")
		// err2 := cmd.Run()
		// log.Println(err2)
		log.Fatal(err)
	}
	
	
	// if len(os.Args) > 1 {
	// 	return fmt.Errorf("unknown argument %q (this program should be run by protoc, not directly)", os.Args[1])
	// }
	// in, err := ioutil.ReadAll(os.Stdin)
	// if err != nil {
	// 	return err
	// }
	// read pb.bin file
	in, err := os.ReadFile("./code_generator_request.pb.bin")
	if err != nil {
		log.Fatal("unable to read input: ", err)
	}
	
	req := &pluginpb.CodeGeneratorRequest{}
	if err := proto.Unmarshal(in, req); err != nil {
		return err
	}
	
	req.Parameter = nil
	
	gen, err := opts.New(req)
	if err != nil {
		return err
	}
	if err := f(gen); err != nil {
		// Errors from the plugin function are reported by setting the
		// error field in the CodeGeneratorResponse.
		//
		// In contrast, errors that indicate a problem in protoc
		// itself (unparsable input, I/O errors, etc.) are reported
		// to stderr.
		gen.Error(err)
	}
	resp := gen.Response()
	
	for _, file := range resp.File{
		fileName := *file.Name
		strs := strings.Split(fileName, "/")
		fileName = strs[len(strs)-1]
		err = os.WriteFile(fileName, []byte(*file.Content), 0644)
		if err != nil {
			return err
		}
	}
	return nil
	
	// // protoc-gen-debug supports proto3 field presence for testing purposes
	// var supportedFeatures = uint64(pluginpb.CodeGeneratorResponse_FEATURE_PROTO3_OPTIONAL)
	// if data, err = proto.Marshal(&plugin_go.CodeGeneratorResponse{
	// 	SupportedFeatures: &supportedFeatures,
	// }); err != nil {
	// 	log.Fatal("unable to marshal response payload: ", err)
	// }
	//
	// _, err = io.Copy(os.Stdout, bytes.NewReader(data))
	// if err != nil {
	// 	log.Fatal("unable to write response to stdout: ", err)
	// }
}