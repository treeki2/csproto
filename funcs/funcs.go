package funcs

import (
  "encoding/json"
  "fmt"
  "os"
  "path/filepath"
  "strings"
  
  "github.com/huandu/xstrings"
)

// string calls .String() on v
func String(v interface{}) string {
  switch tv := v.(type) {
  case fmt.Stringer:
    return tv.String()
  default:
    return fmt.Sprintf("%s", v)
  }
}
// json converts v to JSON by calling json.Marshal()
// . if marshaling fails, the error text is returned instead
func Json(v interface{}) string {
  a, err := json.Marshal(v)
  if err != nil {
    return err.Error()
  }
  return string(a)
}
// prettyjson converts v to formatted JSON by calling json.MarshalIndent()
// . if marshaling fails, the error text is returned instead
func Prettyjson(v interface{}) string {
  a, err := json.MarshalIndent(v, "", "  ")
  if err != nil {
    return err.Error()
  }
  return string(a)
}
// splitTrimEmpty splits s on sep then returns the result, excluding empty items
func SplitTrimEmpty(sep string, s string) []string {
  tokens := strings.Split(s, sep)
  result := tokens[:0]
  for _, t := range tokens {
    if t != "" {
      result = append(result, t)
    }
  }
  return result
}
// first returns the first element of a, or "" if a is empty
func First(a []string) string {
  if len(a) == 0 {
    return ""
  }
  return a[0]
}
// last returns the last element of a, or "" if a is empty
func Last(a []string) string {
  if len(a) == 0 {
    return ""
  }
  return a[len(a)-1]
}
// concat appends one or more strings onto the end of a
func Concat(a string, b ...string) string {
  return strings.Join(append([]string{a}, b...), "")
}
// join concatenates the strings in a using sep as the element separator
func Join(sep string, a ...string) string {
  return strings.Join(a, sep)
}
// upperFirst returns s with the first character converted to upper-case
func UpperFirst(s string) string {
  switch len(s) {
  case 0:
    return ""
  case 1:
    return strings.ToUpper(s)
  default:
    return strings.ToUpper(s[:1]) + s[1:]
  }
}
// lowerFirst returns s with the first character converted to lower-case
func LowerFirst(s string) string {
  switch len(s) {
  case 0:
    return ""
  case 1:
    return strings.ToLower(s)
  default:
    return strings.ToLower(s[:1]) + s[1:]
  }
}
// camelCase converts s to camel-case
func CamelCase(s string) string {
  switch len(s) {
  case 0:
    return ""
  case 1:
    return strings.ToUpper(s)
  default:
    return xstrings.ToCamelCase(s)
  }
}
// lowerCamelCase converts s to camel-case, then converts the first character to lower-case
func LowerCamelCase(s string) string {
  switch len(s) {
  case 0:
    return ""
  case 1:
    return strings.ToLower(s)
  default:
    s = xstrings.ToCamelCase(s)
    return strings.ToLower(s[:1]) + s[1:]
  }
}
// snakeCase converts s to snake-case
var  SnakeCase = xstrings.ToSnakeCase

// kebabCase converts s to kebab-case
func KebabCase(s string) string {
  return strings.ReplaceAll(xstrings.ToSnakeCase(s), "_", "-")
}
// contains returns true if sub is a substring of s and false if not
func Contains(sub, s string) bool {
  return strings.Contains(s, sub)
}
// trim removes all characters in cutset from s
func Trim(cutset, s string) string {
  return strings.Trim(s, cutset)
}
// ltrim removes all leading characters in cutset from s
func Ltrim(cutset, s string) string {
  return strings.TrimLeft(s, cutset)
}
// rtrim removes all trailing characters in cutset from s
func Rtrim(cutset, s string) string {
  return strings.TrimRight(s, cutset)
}
// trimspace removes all leading and trailing whitespace from s
var Trimspace = strings.TrimSpace
// pathDir returns the directory/folder portion of a file path string
var PathDir = filepath.Dir
// pathBase returns the filename portion of a file path string
var PathBase = filepath.Base
// pathFileName returns the file name portion of a file path string without the extension
func pathFileName(s string) string {
  return strings.TrimSuffix(filepath.Base(s), filepath.Ext(s))
}
// pathExt returns the file extension portion of a file path string
var PathExt = filepath.Ext
// pathClean "cleans" a file path string (see filepath.Clean for details)
var PathClean = filepath.Clean
// absPath converts a file path string to an absolute path
var AbsPath = filepath.Abs
// getenv returns the value of an environment variable
var Getenv = os.Getenv


