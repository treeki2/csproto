package templates

import (
  "fmt"
  "sort"
  "strings"
  "time"
  
  "github.com/Masterminds/sprig/v3"
  "google.golang.org/protobuf/compiler/protogen"
  "google.golang.org/protobuf/reflect/protoreflect"
)

// map of protobuf field kind to the corresponding encoder method
var protoNumberEncodeMethodMap = map[string]string{
  "bool":    "EncodeBool",
  "uint32":  "EncodeUInt32",
  "uint64":  "EncodeUInt64",
  "int32":   "EncodeInt32",
  "int64":   "EncodeInt64",
  "sint32":  "EncodeSInt32",
  "sint64":  "EncodeSInt64",
  "fixed32": "EncodeFixed32",
  "fixed64": "EncodeFixed64",
  "float":   "EncodeFloat32",
  "double":  "EncodeFloat64",
}

func SprigTxtFunc(fname string) func(string) string {
  var f func(string) string
  f = sprig.FuncMap()[fname].(func(string) string)
  return f
}

type MessageTpl struct {
  *protogen.Message
  
  Name       string
  APIVersion string
  
  // func getSafeFieldName(names SpecialNames) func(string) string {
  SpecialNames SpecialNames
  // func (mTpl *MessageTpl) getAdditionalImports(protoFile *protogen.File) func(v interface{}) []string {
  // func (mTpl *MessageTpl) mapFieldGoType(protoFile *protogen.File) func(*protogen.Field) string {
  ProtoFile *protogen.File
  
  Now                time.Time
  Pwd                string
  EnableUnsafeDecode bool
  
  anyMessageHasRequiredFields bool
  extensionDict map[protogen.GoIdent][]*protogen.Field
}

func NewMessageTpl(
    name string,
    protoAPIVersion string,
    msg *protogen.Message,
    protoFile *protogen.File,
    names SpecialNames,
) *MessageTpl {
  
  mTpl := &MessageTpl{
    Name:         name,
    APIVersion:   protoAPIVersion,
    Message:      msg,
    ProtoFile:    protoFile,
    SpecialNames: names,
  }
  
  anyMessageHasRequiredFields := false
  if protoFile.Desc.Syntax() == protoreflect.Proto2 {
    for _, m := range AllMessages(protoFile) {
      anyMessageHasRequiredFields = anyMessageHasRequiredFields || mTpl.msgHasRequiredField(m)
    }
  }
  
  mTpl.anyMessageHasRequiredFields = anyMessageHasRequiredFields
  
  // build a lookup of all extensions keyed by the extendee
  mTpl.extensionDict = make(map[protogen.GoIdent][]*protogen.Field)
  for _, m := range protoFile.Messages {
    for _, f := range m.Extensions {
      mTpl.extensionDict[f.Extendee.GoIdent] = append(mTpl.extensionDict[f.Extendee.GoIdent], f)
    }
  }
  
  return mTpl
}

// protoNumberEncodeMethod returns the name of the method on csproto.Encoder that should be called
// to write a given numeric field type using normal or packed encoding (based on the value of packed).
func (mTpl *MessageTpl) protoNumberEncodeMethod(typ string, packed bool) string {
  m, exists := protoNumberEncodeMethodMap[typ]
  if !exists {
    return "XXX_UnknownType_XXX"
  }
  if packed {
    m = strings.ReplaceAll(m, "Encode", "EncodePacked")
  }
  return m
}

// getExtensions returns a list of fields that are proto2 extensions for the specified message
func (mTpl *MessageTpl) getExtensions(msg *protogen.Message) []*protogen.Field {
  exts := mTpl.extensionDict[msg.GoIdent]
  return exts
}


// getAdditionalImports returns a list of distinct imports paths required by the fields of v, which
// must be either a single protogen.Message or a slice of messages.
func (mTpl *MessageTpl) getAdditionalImportsfunc(v interface{}) []string {
  paths := make(map[string]struct{})
  switch tv := v.(type) {
  case *protogen.Message:
    for _, p := range mTpl.additionalImportsForType(mTpl.ProtoFile.GoImportPath, tv) {
      paths[p] = struct{}{}
    }
  case []*protogen.Message:
    for _, m := range tv {
      for _, p := range mTpl.additionalImportsForType(mTpl.ProtoFile.GoImportPath, m) {
        paths[p] = struct{}{}
      }
    }
  default:
  }
  res := make([]string, 0, len(paths))
  for k := range paths {
    res = append(res, k)
  }
  sort.Strings(res)
  return res
}

// additionalImportsForType returns a list of import paths referenced by the fields of m that are
// distinct from the package declared by p.
func (mTpl *MessageTpl) additionalImportsForType(p protogen.GoImportPath, m *protogen.Message) []string {
  var res []string
  for _, fld := range m.Fields {
    switch fld.Desc.Kind() {
    case protoreflect.MessageKind:
      if ip := fld.Message.GoIdent.GoImportPath; ip != p {
        res = append(res, ip.String())
      }
    case protoreflect.EnumKind:
      if ip := fld.Enum.GoIdent.GoImportPath; ip != p {
        res = append(res, ip.String())
      }
    default:
      // nothing to do
    }
  }
  return res
}

// getImportPrefix returns the package import prefix for a given message type, or an empty string
// for messages in the same package as protoFile.
//
// Ex: For a field called Ts that is a google.protobuf.timestamp.Timestamp, the Go package is
// "google.golang.org/protobuf/types/known/timestamppb" so a local variable must be declared as timestamppb.Ts.
// This function returns "timestamppb.".
func (mTpl *MessageTpl) getImportPrefix(v interface{}) string {
  switch tv := v.(type) {
  case *protogen.Message:
    if tv.GoIdent.GoImportPath != mTpl.ProtoFile.GoImportPath {
      toks := strings.Split(string(tv.GoIdent.GoImportPath), "/")
      return toks[len(toks)-1] + "."
    }
  case *protogen.Enum:
    if tv.GoIdent.GoImportPath != mTpl.ProtoFile.GoImportPath {
      toks := strings.Split(string(tv.GoIdent.GoImportPath), "/")
      return toks[len(toks)-1] + "."
    }
  default:
    return ""
  }
  return ""
}

// mapFieldGoType returns the Go type definition for a given field descriptor that represents a map entry
func (mTpl *MessageTpl) mapFieldGoType (field *protogen.Field) string {
  if !field.Desc.IsMap() {
    return "<<invalid>> /* field is not a map entry */"
  }
  kd, vd := field.Desc.MapKey(), field.Desc.MapValue()
  var ktype, vtype string
  switch kd.Kind() {
  case protoreflect.Int32Kind, protoreflect.Sint32Kind, protoreflect.Sfixed32Kind:
    ktype = "int32"
  case protoreflect.Int64Kind, protoreflect.Sint64Kind, protoreflect.Sfixed64Kind:
    ktype = "int64"
  case protoreflect.Uint32Kind, protoreflect.Fixed32Kind:
    ktype = "uint32"
  case protoreflect.Uint64Kind, protoreflect.Fixed64Kind:
    ktype = "uint64"
  case protoreflect.StringKind:
    ktype = "string"
  default:
    ktype = fmt.Sprintf("<<invalid>> /*%v*/", kd.Kind())
  }
  
  switch vd.Kind() {
  case protoreflect.Int32Kind, protoreflect.Sint32Kind, protoreflect.Sfixed32Kind:
    vtype = "int32"
  case protoreflect.Int64Kind, protoreflect.Sint64Kind, protoreflect.Sfixed64Kind:
    vtype = "int64"
  case protoreflect.Uint32Kind, protoreflect.Fixed32Kind:
    vtype = "uint32"
  case protoreflect.Uint64Kind, protoreflect.Fixed64Kind:
    vtype = "uint64"
  case protoreflect.FloatKind:
    vtype = "float32"
  case protoreflect.DoubleKind:
    vtype = "float64"
  case protoreflect.StringKind:
    vtype = "string"
  case protoreflect.BytesKind:
    vtype = "[]byte"
  case protoreflect.BoolKind:
    vtype = "bool"
  case protoreflect.EnumKind:
    // TODO: dbourque - 2022-04-08
    // is the value field always the 2nd item?  or do we need to loop and check the
    // number on the descriptor?
    f := field.Message.Fields[1]
    vtype = mTpl.getImportPrefix(f.Enum) + f.Enum.GoIdent.GoName
  case protoreflect.MessageKind:
    // TODO: dbourque - 2022-04-08
    // is the value field always the 2nd item?  or do we need to loop and check the
    // number on the descriptor?
    f := field.Message.Fields[1]
    vtype = "*" + mTpl.getImportPrefix(f.Message) + f.Message.GoIdent.GoName
  default:
    vtype = fmt.Sprintf("<<invalid>> /*%v*/", vd.Kind())
  }
  return fmt.Sprintf("map[%s]%s", ktype, vtype)
}

// msgHasRequiredField returns true if the specified message has at least 1 field marked required and
// false if not
func (mTpl *MessageTpl) msgHasRequiredField(m *protogen.Message) bool {
  if m.Desc.Syntax() == protoreflect.Proto3 {
    return false
  }
  for _, f := range m.Fields {
    if f.Desc.Cardinality() == protoreflect.Required {
      return true
    }
  }
  return false
}

// hasRequiredFields returns true if at least one field in the specified message is marked required
// and false if not.
//
// If m is nil, this function returns true if *any* message in the Protobuf file has a required field
// and false if not.
func (mTpl *MessageTpl) hasRequiredFields(m *protogen.Message) bool {
  if m == nil {
    return mTpl.anyMessageHasRequiredFields
  }
  if m.Desc.Syntax() == protoreflect.Proto3 {
    return false
  }
  for _, f := range m.Fields {
    if f.Desc.Cardinality() == protoreflect.Required {
      return true
    }
  }
  return false
}

// getSafeFieldName translates field names that will collide with standard Protobuf methods to a
// corresponding "safe" name by appending an underscore.
//
// This translation is done to align with how Gogo Protobuf handles the same situation.
func (mTpl *MessageTpl) getSafeFieldName(name string) string {
  if mTpl.SpecialNames.IsSpecial(name) {
    return name + "_"
  }
  return name
}


func (mTpl *MessageTpl) trunc(c int, s string) string {
  if c < 0 && len(s)+c > 0 {
    return s[len(s)+c:]
  }
  if c >= 0 && len(s) > c {
    return s[:c]
  }
  return s
}