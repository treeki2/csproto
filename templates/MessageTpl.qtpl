{% import (
)
%}

{% code


%}

{% func (mTpl *MessageTpl) Size() %}
// Size calculates and returns the size, in bytes, required to hold the contents of m using the Protobuf
// binary encoding.
func (m *{%s mTpl.Message.GoIdent.GoName%}) Size() int {
    // nil message is always 0 bytes
  if m == nil {
    return 0
  }
  // return cached size, if present
  {% if mTpl.APIVersion == "v1" -%}
      if csz := int(atomic.LoadInt32(&m.XXX_sizecache)); csz > 0 {
  {%- else -%}
      if csz := int(atomic.LoadInt32(&m.sizeCache)); csz > 0 {
  {%- endif %}
          return csz
      }
  // calculate and cache
  var sz, l int
  _ = l // avoid unused variable

  {% for _, field := range mTpl.Message.Fields%}
      {%if field.Desc.ContainingOneof() == nil %}
          //{% collapsespace %} {%s field.GoName%} ({%s field.Desc.Kind().String()%},{%s field.Desc.Cardinality().String()%}
          {%if field.Desc.IsPacked() %},packed{% endif %})
          {% endcollapsespace %}
          {%code mTpl.SizeOfField(field)%}
      {%- endif -%}
  {% endfor %}

  {% for _, oneof := range mTpl.Message.Oneofs %}
      {%code mTpl.SizeOfOneOf(oneof)%}
  {% endfor %}

  {%- if mTpl.Message.Desc.Syntax().String() == "proto2" -%}
  {% for _, ext := range mTpl.getExtensions(mTpl.Message)%}
      {%= mTpl.SizeOfExtension(ext)%}
  {% endfor %}
  {%- endif -%}

  // cache the size so it can be re-used in Marshal()/MarshalTo()
  {% if mTpl.APIVersion == "v1" -%}
      atomic.StoreInt32(&m.XXX_sizecache, int32(sz))
  {%- else -%}
      atomic.StoreInt32(&m.sizeCache, int32(sz))
  {%- endif %}
  return sz
}
{% endfunc %}

{% func (mTpl *MessageTpl) Marshal() %}
// Marshal converts the contents of m to the Protobuf binary encoding and returns the result or an error.
func (m *{%s= mTpl.Message.GoIdent.GoName %}) Marshal() ([]byte, error) {
    siz := m.Size()
    buf := make([]byte, siz)
    err := m.MarshalTo(buf)
    return buf, err
}
{% endfunc %}

{% func (mTpl *MessageTpl) MarshalTo() %}
// MarshalTo converts the contents of m to the Protobuf binary encoding and writes the result to dest.
func (m *{%s= mTpl.Message.GoIdent.GoName %}) MarshalTo(dest []byte) error {
	var (
    	enc = csRuntime.NewEncoder(dest)
		buf []byte
		err error
        extVal interface{}
	)
    // ensure no unused variables
    _ = enc
    _ = buf
    _ = err
    _ = extVal
    {% for _, field := range mTpl.Message.Fields %}
        {%- if field.Desc.ContainingOneof() == nil %}
            // {%s= field.GoName %} ({%d int(field.Desc.Number())%},{%if field.Desc.IsMap()%}map{%else%}{%s= field.Desc.Kind().String()%},{%s= field.Desc.Cardinality().String()%}{%if field.Desc.IsPacked() %},packed{% endif %}{%endif%})
            {%= mTpl.MarshalField(field) %}
        {%- endif -%}
    {% endfor %}
    {% for _, oneof := range mTpl.Message.Oneofs -%}
        // mTpl.Message.GoName%} (oneof)
        {%= mTpl.MarshalOneOf(oneof) -%}
    {% endfor %}

    {%- if mTpl.Message.Desc.Syntax().String() == "proto2" -%}
    {% for _, ext := range mTpl.getExtensions(mTpl.Message)%}
        {%= mTpl.MarshalExtension(ext)%}
    {% endfor %}
    {%- endif -%}

    return nil
}
{%- endfunc -%}


{% func (mTpl *MessageTpl) Unmarshal() %}
// Unmarshal decodes a binary encoded Protobuf message from p and populates m with the result.
func (m *{%s= mTpl.Message.GoIdent.GoName%}) Unmarshal(p []byte) error {
    if len(p) == 0 {
        return fmt.Errorf("cannot unmarshal from an empty buffer")
    }
    // clear any existing data
    m.Reset()
    dec := csRuntime.NewDecoder(p)
    {%if mTpl.EnableUnsafeDecode -%}
    // enable faster, but unsafe, string decoding
    dec.SetMode(csRuntime.DecoderModeFast)
    {%endif -%}
    for dec.More() {
		tag, wt, err := dec.DecodeTag()
		if err != nil {
			return err
		}
		switch tag {

        {%- for _, field := range mTpl.Message.Fields -%}
        {%- if field.Desc.ContainingOneof() == nil -%}
        case {%d= int(field.Desc.Number()) %}: // {%s= field.GoName %} ({%if field.Desc.IsMap()%}map{%else%}{%s= field.Desc.Kind().String()%},{%s= field.Desc.Cardinality().String()%}{%if field.Desc.IsPacked() %},packed{% endif %}{%endif%})
        {%=  mTpl.UnmarshalField(field) %}
        {%- endif -%}
        {%- endfor -%}

        {% for _, field := range mTpl.Message.Oneofs -%}
        {%=  mTpl.UnmarshalOneOf(field) -%}
        {% endfor %}

        {%- if mTpl.Message.Desc.Syntax().String() == "proto2" -%}
        {% for _, ext := range mTpl.getExtensions(mTpl.Message)%}
        {%= mTpl.UnmarshalExtension(ext)%}
        {% endfor %}
        {%- endif -%}

        default:
            if skipped, err := dec.Skip(tag, wt); err != nil {
                return fmt.Errorf("invalid operation skipping tag %v: %w", tag, err)
            } else {
            {%- if mTpl.APIVersion == "v1" -%}
                m.XXX_unrecognized = append(m.XXX_unrecognized, skipped...)
            {%- elseif mTpl.APIVersion == "v2" -%}
                m.unknownFields = append(m.unknownFields, skipped...)
            {%- else -%}
                panic(fmt.Errorf("unknown/unsupported protobuf runtime '{%s= mTpl.APIVersion %}'"))
            {%- endif -%}
            }
        }
    }

    {%if mTpl.hasRequiredFields(mTpl.Message) %}
    // verify required fields are assigned
    if err := m.csprotoCheckRequiredFields(); err != nil {
        return err
    }
    {%endif%}
    return nil
}
{% endfunc %}

{% func (mTpl *MessageTpl) csprotoCheckRequiredFields() %}
{%if mTpl.hasRequiredFields(mTpl.Message)%}
    // csprotoCheckRequiredFields is called by Unmarshal() to ensure that all required fields have been
    // populated.
    func (m *{%s= mTpl.Message.GoIdent.GoName%}) csprotoCheckRequiredFields() error {
        var missingFields []string
    {% for _, field := range mTpl.Message.Fields %}
    {% if field.Desc.Cardinality().String() == "required" && field.Desc.Syntax().String() == "proto2" -%}
        if m.{%s= mTpl.getSafeFieldName(field.GoName) %} == nil {
            missingFields = append(missingFields, "{%s= mTpl.getSafeFieldName(field.GoName) %}")
        }
    {%- endif -%}
    {% endfor %}
        if len(missingFields) > 0 {
            var sb strings.Builder
            sb.WriteString("cannot unmarshal, one or more required fields missing: ")
            for i, s := range missingFields {
                if i > 0 {
                    sb.WriteRune(',')
                }
                sb.WriteString(s)
            }
            return fmt.Errorf(sb.String())
        }
        return nil
   }
{% endif %}
{%endfunc%}