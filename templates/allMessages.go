package templates

import (
  "google.golang.org/protobuf/compiler/protogen"
)

// allMessages returns a list of all top-level and nested message definitions in protoFile
func AllMessages(protoFile *protogen.File) []*protogen.Message {
  var queue, msgs []*protogen.Message
  queue = append(queue, protoFile.Messages...)
  for len(queue) > 0 {
    m := queue[0]
    queue = queue[1:]
    msgs = append(msgs, m)
    for _, mm := range m.Messages {
      // skip "messgaes" that represent map fields
      if mm.Desc.IsMapEntry() {
        continue
      }
      queue = append(queue, mm)
    }
  }
  
  return msgs
}
