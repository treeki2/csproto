// Code generated by qtc from "head.qtpl". DO NOT EDIT.
// See https://github.com/valyala/quicktemplate for details.

//line head.qtpl:1
package templates

//line head.qtpl:1
import (
	qtio422016 "io"

	qt422016 "github.com/valyala/quicktemplate"
)

//line head.qtpl:1
var (
	_ = qtio422016.Copy
	_ = qt422016.AcquireByteBuffer
)

//line head.qtpl:1
func StreamHead(qw422016 *qt422016.Writer, packageName string) {
//line head.qtpl:1
	qw422016.N().S(`

package `)
//line head.qtpl:3
	qw422016.N().S(packageName)
//line head.qtpl:3
	qw422016.N().S(`

import (
  "fmt"
  "sync/atomic"
  "gd/cmd/csproto/csRuntime"
)
`)
//line head.qtpl:10
}

//line head.qtpl:10
func WriteHead(qq422016 qtio422016.Writer, packageName string) {
//line head.qtpl:10
	qw422016 := qt422016.AcquireWriter(qq422016)
//line head.qtpl:10
	StreamHead(qw422016, packageName)
//line head.qtpl:10
	qt422016.ReleaseWriter(qw422016)
//line head.qtpl:10
}

//line head.qtpl:10
func Head(packageName string) string {
//line head.qtpl:10
	qb422016 := qt422016.AcquireByteBuffer()
//line head.qtpl:10
	WriteHead(qb422016, packageName)
//line head.qtpl:10
	qs422016 := string(qb422016.B)
//line head.qtpl:10
	qt422016.ReleaseByteBuffer(qb422016)
//line head.qtpl:10
	return qs422016
//line head.qtpl:10
}
