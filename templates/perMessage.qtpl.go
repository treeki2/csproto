// Code generated by qtc from "perMessage.qtpl". DO NOT EDIT.
// See https://github.com/valyala/quicktemplate for details.

//line perMessage.qtpl:2
package templates

//line perMessage.qtpl:2
import (
	qtio422016 "io"

	qt422016 "github.com/valyala/quicktemplate"
)

//line perMessage.qtpl:2
var (
	_ = qtio422016.Copy
	_ = qt422016.AcquireByteBuffer
)

//line perMessage.qtpl:2
func (mTpl *MessageTpl) StreamPerMesssage(qw422016 *qt422016.Writer, packageName string) {
//line perMessage.qtpl:2
	qw422016.N().S(`
  `)
//line perMessage.qtpl:3
	mTpl.StreamSize(qw422016)
//line perMessage.qtpl:3
	qw422016.N().S(`
  `)
//line perMessage.qtpl:4
	mTpl.StreamMarshal(qw422016)
//line perMessage.qtpl:4
	qw422016.N().S(`
  `)
//line perMessage.qtpl:5
	mTpl.StreamMarshalTo(qw422016)
//line perMessage.qtpl:5
	qw422016.N().S(`
  `)
//line perMessage.qtpl:6
	mTpl.StreamUnmarshal(qw422016)
//line perMessage.qtpl:6
	qw422016.N().S(`
  `)
//line perMessage.qtpl:7
	mTpl.streamcsprotoCheckRequiredFields(qw422016)
//line perMessage.qtpl:7
	qw422016.N().S(`
`)
//line perMessage.qtpl:8
}

//line perMessage.qtpl:8
func (mTpl *MessageTpl) WritePerMesssage(qq422016 qtio422016.Writer, packageName string) {
//line perMessage.qtpl:8
	qw422016 := qt422016.AcquireWriter(qq422016)
//line perMessage.qtpl:8
	mTpl.StreamPerMesssage(qw422016, packageName)
//line perMessage.qtpl:8
	qt422016.ReleaseWriter(qw422016)
//line perMessage.qtpl:8
}

//line perMessage.qtpl:8
func (mTpl *MessageTpl) PerMesssage(packageName string) string {
//line perMessage.qtpl:8
	qb422016 := qt422016.AcquireByteBuffer()
//line perMessage.qtpl:8
	mTpl.WritePerMesssage(qb422016, packageName)
//line perMessage.qtpl:8
	qs422016 := string(qb422016.B)
//line perMessage.qtpl:8
	qt422016.ReleaseByteBuffer(qb422016)
//line perMessage.qtpl:8
	return qs422016
//line perMessage.qtpl:8
}
