#!/usr/bin/env bash


for file in fieldsnippets.tmpl; do
    sed -i "s/{{\/\*/\n\/\*/g" $file
    sed -i "s/\*\/}}/\*\//g" $file
    sed -i "s/{{/{%/g" $file
    sed -i "s/}}/%}/g" $file

    sed -i "s/if ne (\(.*\) | string) \"\(.*\)\"/if field\1\(\).String\(\) != \"\2\"/g" $file
    sed -i "s/if eq (\(.*\) | string) \"\(.*\)\"/if field\1\(\).String\(\) == \"\2\"/g" $file


    sed -i "s/\(.*\)if eq \([^[:space:]]*\) \(.*\)/\1if \2 == \3/g" $file

    sed -i "s/{% define \"\(.*\)\" %}/{%func \(mTpl \*MessageTpl\) \1\(field \*protogen\.Field\) %}/g" $file
    sed -i "s/ template \"\(.*\)\"\(.*\)/= \1(field)\2/g" $file
    sed -i "s/template \"\(.*\)\"\(.*\)/= \1(field)\2/g" $file

    sed -i "s/\(.*\){%\(.*\) | \(.*\)%}/\1{%s= mTpl\.\3\(\2\) %}/g" $file
    sed -i "s/(\.\(.*\))/\(field\.\1\)/g" $file
    sed -i "s/{%\.\(.*\)%}/{%s= field\.\1\(\)%}/g" $file
    sed -i "s/{%- else if/{%- elseif/g" $file
    sed -i "s/{%end()%}/{%endif%}/g" $file

    sed -i "s/csproto\./csRuntime\./g" $file
done