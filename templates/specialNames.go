package templates

import (
  "sort"
  "strings"
)

// SpecialNames defines a flag that holds a set of unique strings presenting Protobuf field names
// that are "special" in that they will collide with generated methods on Protobuf message types.
// "Size" is a common example.
type SpecialNames map[string]struct{}

// String returns a string representation of v, or the string "<none>" if v is nil or empty
func (v SpecialNames) String() string {
  if len(v) == 0 {
    return "<none>"
  }
  names := make([]string, 0, len(v))
  for k := range v {
    names = append(names, k)
  }
  sort.Strings(names)
  return strings.Join(names, ",")
}

// Set adds one or more values to the set names.
//
// s should contain a comma-delimited list and any leading or trailing whitespace is trimmed from
// each name token
func (v *SpecialNames) Set(s string) error {
  names := strings.Split(s, ",")
  for _, n := range names {
    n = strings.TrimSpace(n)
    if n == "" {
      continue
    }
    (*v)[n] = struct{}{}
  }
  return nil
}

// IsSpecial returns true if n is in the set and false otherwise.
func (v SpecialNames) IsSpecial(n string) bool {
  if len(v) == 0 {
    return false
  }
  _, found := v[n]
  return found
}

