
#

##

// According to https://go.dev/blog/protobuf-apiv2

The github.com/golang/protobuf module is APIv1.

The google.golang.org/protobuf module is APIv2

google.golang.org/protobuf@v1.20.0 is APIv2. This module depends upon github.com/golang/protobuf@v1.4.0, so any program which uses APIv2 will automatically pick a version of APIv1 which integrates with it.
