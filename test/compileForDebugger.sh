#!/usr/bin/env bash

#go get -u google.golang.org/protobuf/{proto,protoc-gen-go}
# Note: Don't use github/google,...

export GOPATH=$HOME/workspaces/gopath
export GOBIN=$HOME/workspaces/gopath/vendor/bin
export PATH=$PATH:${GOROOT}/bin:$GOBIN
export GO_PROTOBUF
export GOGO_PROTOBUF=${GOPATH}/src
export SRC_ROOT=./

# check: https://github.com/lyft/protoc-gen-star/blob/master/protoc-gen-debug/main.go
protoc \
  --plugin=protoc-gen-debug=$GOPATH/bin/protoc-gen-debug \
  --debug_out=".:." \
  *.proto
