#!/usr/bin/env bash


#go get -u google.golang.org/protobuf/{proto,protoc-gen-go}
# Note: Don't use github/google,...

# Not suggest to use this:
#sudo apt install protobuf-compiler //debian package (compilered from c++)

export GOPATH=$HOME/workspaces/gopath
export GOBIN=$HOME/workspaces/gopath/vendor/bin
export PATH=$PATH:${GOROOT}/bin:$GOBIN
export GO_PROTOBUF
export GOGO_PROTOBUF=${GOPATH}/src
export SRC_ROOT=./

# go get -u github.com/gogo/protobuf  #-u means update
# go install github.com/gogo/protobuf/protoc-gen-gogo
# go install github.com/gogo/protobuf/protoc-gen-gofast

protoc \
    --proto_path=./ \
    --proto_path=${GOPATH}/src \
    --proto_path=${GOPATH}/vendor/protobuf/src \
    --proto_path=${GODUCK_PATH} \
    --gogo_out=plugins=grpc:. \
    *.proto

#--gogofaster_out=plugins=grpc:. \