package test

import (
	"fmt"
	"gd/cmd/csproto/csRuntime"
	"sync/atomic"
)

// Size calculates and returns the size, in bytes, required to hold the contents of m using the Protobuf
// binary encoding.
func (m *MSG_TEST) Size() int {
	// nil message is always 0 bytes
	if m == nil {
		return 0
	}
	// return cached size, if present
	if csz := int(atomic.LoadInt32(&m.XXX_sizecache)); csz > 0 {

		return csz
	}
	// calculate and cache
	var sz, l int
	_ = l // avoid unused variable

	// EpocTime (int64,optional )

	// Echo (string,optional )

	// cache the size so it can be re-used in Marshal()/MarshalTo()
	atomic.StoreInt32(&m.XXX_sizecache, int32(sz))

	return sz
}

// Marshal converts the contents of m to the Protobuf binary encoding and returns the result or an error.
func (m *MSG_TEST) Marshal() ([]byte, error) {
	siz := m.Size()
	buf := make([]byte, siz)
	err := m.MarshalTo(buf)
	return buf, err
}

// MarshalTo converts the contents of m to the Protobuf binary encoding and writes the result to dest.
func (m *MSG_TEST) MarshalTo(dest []byte) error {
	var (
		enc    = csRuntime.NewEncoder(dest)
		buf    []byte
		err    error
		extVal interface{}
	)
	// ensure no unused variables
	_ = enc
	_ = buf
	_ = err
	_ = extVal

	// EpocTime (1,int64,optional)

	if m.EpocTime != 0 {
		enc.EncodeInt64(1, m.EpocTime)
	}

	// Echo (2,string,optional)

	if len(m.Echo) > 0 {
		enc.EncodeString(2, m.Echo)
	}

	return nil
}

// Unmarshal decodes a binary encoded Protobuf message from p and populates m with the result.
func (m *MSG_TEST) Unmarshal(p []byte) error {
	if len(p) == 0 {
		return fmt.Errorf("cannot unmarshal from an empty buffer")
	}
	// clear any existing data
	m.Reset()
	dec := csRuntime.NewDecoder(p)
	for dec.More() {
		tag, wt, err := dec.DecodeTag()
		if err != nil {
			return err
		}
		switch tag {

		case 1: // EpocTime (int64,optional)

			if wt != csRuntime.WireTypeVarint {
				return fmt.Errorf("incorrect wire type %v for tag field 'EpocTime' (tag=1), expected 0 (varint)", wt)
			}
			if v, err := dec.DecodeInt64(); err != nil {
				return fmt.Errorf("unable to decode int64 value for field 'EpocTime' (tag=1): %w", err)
			} else {
				m.EpocTime = v
			}

		case 2: // Echo (string,optional)

			if wt != csRuntime.WireTypeLengthDelimited {
				return fmt.Errorf("incorrect wire type %v for field 'Echo' (tag=2), expected 2 (length-delimited)", wt)
			}
			if s, err := dec.DecodeString(); err != nil {
				return fmt.Errorf("unable to decode string value for field 'Echo' (tag=2): %w", err)
			} else {
				m.Echo = s
			}

		default:
			if skipped, err := dec.Skip(tag, wt); err != nil {
				return fmt.Errorf("invalid operation skipping tag %v: %w", tag, err)
			} else {
				m.XXX_unrecognized = append(m.XXX_unrecognized, skipped...)
			}
		}
	}

	return nil
}
