package test

type MSG_TEST struct {
  EpocTime             int64    `protobuf:"varint,1,opt,name=EpocTime,proto3" json:"EpocTime,omitempty"`
  Echo                 string   `protobuf:"bytes,2,opt,name=Echo,proto3" json:"Echo,omitempty"`
  XXX_NoUnkeyedLiteral struct{} `json:"-"`
  XXX_unrecognized     []byte   `json:"-"`
  XXX_sizecache        int32    `json:"-"`
}

func (m *MSG_TEST) Reset()         { *m = MSG_TEST{} }


